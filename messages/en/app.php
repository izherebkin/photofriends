<?php

return [
    'HOME' => 'Home',

    'ABOUT' => 'About',

    'CONTACT' => 'Contact',
    'FILL_OUT_TO_CONTACT' => 'If you have some questions, please fill out the following form to contact us. Thank you.',
    'THANKS_FOR_CONTACTING' => 'Thank you for contacting us. We will respond to you as soon as possible.',

    'PROFILE' => 'Profile',

    'LOGIN' => 'Login',
    'FILL_OUT_TO_LOGIN' => 'Please fill out the following fields to autorize.',
    'REMEMBER_ME' => 'Remember Me',
    'PASSWORD_RESET' => 'Password reset',
    'AUTORIZE' => 'Autorize',
    'USERNAME_OR_PASSWORD_INVALID' => 'Username or password invalid.',
    'ACCOUNT_IS_BLOCKED' => 'Your account is blocked.',
    'ACCOUNT_IS_NOT_ACTIVATED' => 'Your account is not activated.',

    'SIGNUP' => 'Signup',
    'FILL_OUT_TO_SIGNUP' => 'Please fill out the following form to sign up.',
    'REGISTER' => 'Register',
    'USERNAME_ALREADY_TAKEN' => 'This username is already taken.',
    'EMAIL_ALREADY_TAKEN' => 'This email is already taken.',
    'SIGNUP_SUCCESS' => 'To the specified email was sent a letter with a link to activate your account.',
    'EMAIL_CONFIRM_SUCCESS' => 'Thank you! Your account is successfully activated.',
    'EMAIL_CONFIRM_ERROR' => 'Sorry... An account activation error has occured...',

    'PASSWORD_RESET_REQUEST' => 'Password reset request',
    'FILL_OUT_EMAIL' => 'Please fill out your email.',
    'EMAIL_DO_NOT_EXIST' => 'User with such email does not exist.',
    'PASSWORD_RESET_REQUEST_SUCCESS' => 'Thank you! To your email was sent message with link to reset password.',
    'PASSWORD_RESET_REQUEST_ERROR' => 'Sorry... We are having problems with sending emails...' ,

    'PASSWORD_RESETTING' => 'Password resetting',
    'FILL_OUT_NEW_PASSWORD' => 'Please choose your new password.',
    'NEW_PASSWORD_SAVED' => 'Thank you! New password successfully saved.',

    'LOGOUT' => 'Logout',

    'UPLOAD_AVATAR' => 'Upload Avatar',
    'AVATAR_IMAGE_INFO' => 'Avatar image info',
    'IMAGE_MUST_BE_SQUARE' => 'Image must be square. For example, 200px x 200px.',
    'AVAILABLE_IMAGE_FORMATS' => 'Available image formats: png, jpg, jpeg.',
    'IMAGE_WIDTH_LIMIT_IS_200-1000' => 'Image width limit is 200px - 1000px.',
    'IMAGE_HEIGHT_LIMIT_IS_200-1000' => 'Image height limit is 200px - 1000px.',
    'FILE_SIZE_LIMIT_IS_1MB' => 'File size limit is 1MB.',
    'NEW_AVATAR_SAVED' => 'Thank you! New avatar successfully saved.',
    'IT_IS_YOUR_CURRENT_EMAIL' => 'This is your current email. Please fill out new email.',
    'PASSWORD_INVALID' => 'Password invalid.',

    'MY_PHOTOS' => 'My photos',

    'UPLOADING_PHOTOS' => 'Uploading photos',
    'DRAG_N_DROP_IMAGES_OR_CLICK_TO_BROWSE' => 'Drop image files here to upload or click to browse',
    'UPLOADING_IMAGES_INFO' => 'Uploading images info',
    'AVAILABLE_IMAGES_FORMATS' => 'Available images formats: png, jpg, jpeg.',
    'IMAGE_SIZE_LIMIT_IS_5MB' => 'Image file size limit is 5MB.',
    'IMAGES_LIMIT_PER_UPLOAD_IS_20' => 'Images limit per upload is 20.',
    'BROWSER_IS_NOT_SUPPORT_UPLOADING_IMAGES' => 'Your browser does not support uploading images.',
    'IMAGE_SIZE_IS_TOO_BIG' => 'Image file size is too big ({{filesize}}MB). Image file size limit is {{maxFilesize}}MB.',
    'INVALID_FILE_FORMAT' => 'Invalid file format. Valid formats: png, jpg, jpeg.',
    'IMAGES_LIMIT_PER_UPLOAD_IS_REACHED' => 'Images limit per upload is reached.',
    'PROCESSING_IMAGE_FILES_ON_THE_SERVER' => 'Processing image files on the server...',
    'SERVER_ERROR_PROCESSING_IMAGE_FILE' => 'The file "{file}" was not saved due to an error processing the image on the server.',
    'SERVER_ERROR_VALIDATION_FILE' => 'The file "{file}" was not saved due to validation errors on the server.',
    'IMAGE_FILES_UPLOADING_SUCCESS' => 'Uploading image files was successful!',
    'SERVER_ERROR_PROCESSING_SOME_IMAGE_FILES' => 'An error occurred while processing some image files on the server.',
    'SERVER_ERROR_PROCESSING_IMAGE_FILES' => 'An error occurred while processing image files on the server.',

    'EDITING_PHOTOS' => 'Editing photos',

    'USERNAME' => 'Username',
    'EMAIL' => 'Email',
    'PASSWORD' => 'Password',
    'CURRENT_PASSWORD' => 'Current password',
    'NEW_PASSWORD' => 'New password',
    'NEW_PASSWORD_REPEAT' => 'Repeat new password',
    'SUBJECT' => 'Subject',
    'BODY' => 'Body',
    'VERIFY_CODE' => 'Verify code',
    'STATUS' => 'Status',
    'AVATAR' => 'Avatar',
    'CREATED' => 'Created',
    'UPDATED' => 'Updated',
    'USER_ID' => 'User ID',
    'ORIGINAL' => 'Original',
    'THUMBNAIL' => 'Thumbnail',
    'DESCRIPTION' => 'Description',

    'SEND' => 'Send',
    'SAVE' => 'Save',
    'UPDATE' => 'Update',
    'UPLOAD' => 'Upload',
    'EDITING' => 'Editing',
    'DELETE' => 'Delete',

    'STATUS_WAIT' => 'Confirmation pending',
    'STATUS_ACTIVE' => 'Active',
    'STATUS_BLOCKED' => 'Blocked',

    'HELLO {username}' => 'Hello, {username}!',

    'CONFIRM_EMAIL_SUBJECT' => 'Registration account confirmation on the site ',
    'FOLLOW_TO_CONFIRM_EMAIL' => 'Please, follow the link below to confirm your email:',
    'IGNORE_IF_DO_NOT_REGISTER' => 'If you do not register on our site just remove this mail.',

    'RESET_PASSWORD_SUBJECT' => 'Password reset request for an account on the site ',
    'FOLLOW_TO_RESET_PASSWORD' => 'Please, follow the link below to reset your password:',
    'IGNORE_IF_DO_NOT_RESET_PASSWORD' => 'If you do not reset password on our site just remove this mail.',

    'WEBSERVER_REQUEST_ERROR' => 'The above error occurred while the Web server was processing your request.',
    'CONTACT_IF_REQUEST_ERROR_IS_NOT' => 'Please contact us if you think this is a server error. Thank you.',
];