<?php

return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'info@example.com',
    'user.passwordResetTokenExpire' => 3600,
];
