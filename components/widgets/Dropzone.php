<?php

namespace app\components\widgets;


use app\assets\DropzoneAsset;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\Json;
use yii\web\JsExpression;
use yii\web\Request;

/**
 * Yii2 DropzoneJS Widget.
 * DropzoneJS is an open source library that provides drag’n’drop file uploads with image previews.
 * @see http://www.dropzonejs.com/
 * @author Igor Zherebkin
 */
class Dropzone extends Widget
{
    public $dropzoneContainer;

    /**
     * @var array An array of options that are supported by Dropzone
     * @see http://www.dropzonejs.com/#configuration-options
     */
    public $options = [];

    /**
     * @var array An array of client events that are supported by Dropzone
     * @see http://www.dropzonejs.com/#events
     */
    public $events = [];

    /**
     * @var string Plain HTML code.
     */
    public $template;

    public function init()
    {
        parent::init();

        if (empty($this->dropzoneContainer)) {
            throw new InvalidConfigException('Please specify the "dropzoneContainer" property.');
        }
        if(Yii::$app->getRequest()->enableCsrfValidation){
            $this->options['headers'][Request::CSRF_HEADER] = Yii::$app->getRequest()->getCsrfToken();
            $this->options['params'][Yii::$app->getRequest()->csrfParam] = Yii::$app->getRequest()->getCsrfToken();
        }
    }

    public function run()
    {
        $this->registerAssets();
        $this->renderDropzone();
    }

    private function registerAssets()
    {
        $view = $this->getView();
        $view->registerJs('Dropzone.autoDiscover = false; var dropzone = new Dropzone("' . $this->dropzoneContainer . '", ' . Json::encode($this->options) . ');');
        foreach ($this->events as $event => $handler) {
            $handler = new JsExpression($handler);
            $this->getView()->registerJs("dropzone.on('{$event}', {$handler})");
        }
        DropzoneAsset::register($view);
    }

    private function renderDropzone()
    {
        echo $this->template;
    }
}