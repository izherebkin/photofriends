<?php

use yii\db\Migration;

class m170217_161026_alter_user_table_add_avatar extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'avatar', $this->string()->notNull());
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'avatar');
    }
}
