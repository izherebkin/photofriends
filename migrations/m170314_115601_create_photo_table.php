<?php

use yii\db\Migration;

/**
 * Handles the creation of table `photo`.
 */
class m170314_115601_create_photo_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%photo}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'original' => $this->string()->notNull(),
            'thumbnail' => $this->string()->notNull(),
            'description' => $this->string(),
        ], $tableOptions);

        $this->createIndex('idx-photo-user_id', '{{%photo}}', 'user_id');

        $this->addForeignKey('fk-photo-user_id', '{{%photo}}', 'user_id', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%photo}}');
    }
}
