<?php

use app\modules\user\models\ProfileUpdateForm;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\user\models\ProfileUpdateForm */
/* @var $avatar string */
/* @var $username string */

$this->title = Yii::t('app', 'EDITING');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'PROFILE'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$template =
'
<div class="input-group col-xs-12 col-sm-8 col-sm-offset-2 col-lg-6 col-lg-offset-3">
	<label class="input-group-btn">
		<span class="btn btn-primary">
			<i class="glyphicon glyphicon-folder-open"></i>
			{input}
		</span>
	</label>
	<input type="text" class="form-control text-center" disabled placeholder="' . Yii::t('app', 'UPLOAD_AVATAR') . '">
	<label class="input-group-btn">
		<span class="btn btn-primary">
			<i class="glyphicon glyphicon-upload"></i>
			<button name="' . ProfileUpdateForm::SCENARIO_AVATAR . '" type="submit" style="display: none;"></button>
		</span>
	</label>
</div>
<div class="text-center">{error}</div>
';

$this->registerJs(
    "$(document).on('change', 'input[type=file]', function(){
        $(this).parent().parent().parent().find('input[disabled]').val($(this).val().match(/[^\\\\/]*$/)[0]);
    });",
    View::POS_READY
);
?>
<div class="user-profile-update">

    <div class="h1 text-center">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="row">
        <div class="col-xs-6 col-sm-6 col-lg-6">
            <div class="text-center">
                <div class="thumbnail" style="display: inline-block;">
                    <div class="form-group">
                        <?= Html::img($avatar) ?>
                    </div>
                    <div class="form-group text-center text-uppercase">
                        <h4><?= $username ?></h4>
                    </div>
                </div>
            </div>

            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
            <?= $form->field($model, 'avatar', ['template' => $template])->fileInput(['style' => 'display: none;'])->label(false) ?>
            <?php ActiveForm::end() ?>

            <div class="text-center">
                <div class="alert alert-info" style="display: inline-block;">
                    <strong><?= Yii::t('app', 'AVATAR_IMAGE_INFO') ?></strong>
                    <div>&#8226; <?= Yii::t('app', 'IMAGE_MUST_BE_SQUARE') ?></div>
                    <div>&#8226; <?= Yii::t('app', 'AVAILABLE_IMAGE_FORMATS') ?></div>
                    <div>&#8226; <?= Yii::t('app', 'IMAGE_WIDTH_LIMIT_IS_200-1000') ?></div>
                    <div>&#8226; <?= Yii::t('app', 'IMAGE_HEIGHT_LIMIT_IS_200-1000') ?></div>
                    <div>&#8226; <?= Yii::t('app', 'FILE_SIZE_LIMIT_IS_1MB') ?></div>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-lg-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?php $form = ActiveForm::begin(['id' => 'email-change-form']); ?>
                    <?= $form->field($model, 'email') ?>
                    <div class="form-group text-center">
                        <?= Html::submitButton(Yii::t('app', 'SEND'), ['class' => 'btn btn-primary', 'name' => ProfileUpdateForm::SCENARIO_EMAIL]) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-body">
                    <?php $form = ActiveForm::begin(['id' => 'password-change-form']); ?>
                    <?= $form->field($model, 'currentPassword')->passwordInput() ?>
                    <?= $form->field($model, 'newPassword')->passwordInput() ?>
                    <?= $form->field($model, 'newPasswordRepeat')->passwordInput() ?>
                    <div class="form-group text-center">
                        <?= Html::submitButton(Yii::t('app', 'SEND'), ['class' => 'btn btn-primary', 'name' => ProfileUpdateForm::SCENARIO_PASSWORD]) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>

</div>