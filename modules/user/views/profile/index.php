<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\user\models\User */

$this->title = Yii::t('app', 'PROFILE');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-profile-index">

    <div class="form-group">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="row">
        <div class="col-xs-6 col-sm-6 col-lg-6">
            <div class="text-center">
                <div class="thumbnail" style="display: inline-block;">
                    <div class="form-group">
                        <?= Html::img($model->avatar) ?>
                    </div>
                    <div class="form-group text-center text-uppercase">
                        <h4><?= $model->username ?></h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-lg-6">
            <?= DetailView::widget([
                'options' => [
                    'class' => 'table table-bordered detail-view',
                ],
                'model' => $model,
                'attributes' => [
                    [
                        'label' => $model->getAttributeLabel('status'),
                        'value' => $model->getStatusName(),
                    ],
                    'created_at:datetime',
                    'email',
                ],
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-xs-12 col-lg-12">
            <div class="form-group text-center">
                <?= Html::a(Yii::t('app', 'UPDATE'), ['/user/profile/update'], ['class'=>'btn btn-primary']) ?>
            </div>
        </div>
    </div>

</div>