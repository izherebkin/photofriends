<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'MY_PHOTOS');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-photos-index">

    <div class="text-center">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <div id="photos-buttons" class="row form-group">
        <div class="col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-2 col-lg-6 col-lg-offset-3">
            <?= Html::a('<i class="glyphicon glyphicon-picture"></i> ' . Yii::t('app', 'UPLOAD'), ['upload'],
                ['class' => 'btn btn-primary pull-left', 'title' => Yii::t('app', 'UPLOADING_PHOTOS')]) ?>
            <?= Html::a('<i class="glyphicon glyphicon-pencil"></i> ' . Yii::t('app', 'UPDATE'), ['update'],
                ['class' => 'btn btn-primary pull-right', 'title' => Yii::t('app', 'EDITING_PHOTOS')]) ?>
        </div>
    </div>

    <p>
        <?= Html::a(Yii::t('app', 'Create Photo'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemOptions' => ['class' => 'item'],
            'itemView' => function ($model, $key, $index, $widget) {
                return Html::a(Html::encode($model->id), ['view', 'id' => $model->id]);
            },
        ]) ?>
    <?php Pjax::end(); ?>
</div>
