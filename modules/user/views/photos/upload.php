<?php

use app\components\widgets\Dropzone;
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = Yii::t('app', 'UPLOADING_PHOTOS');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'MY_PHOTOS'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$template =
    '
<div class="row">
	<div class="col-xs-12 col-sm-12 col-lg-12">
		<div class="form-group text-center">
			<div id="dropzone" class="dropzone">
			    <div id="info" style="pointer-events: none;">
				    <div class="text-uppercase h4">' . Yii::t('app', 'DRAG_N_DROP_IMAGES_OR_CLICK_TO_BROWSE') . '</div>
                    <div class="small">
                        <strong>' . Yii::t('app', 'UPLOADING_IMAGES_INFO') . '</strong>
                        <div>&#8226; ' . Yii::t('app', 'AVAILABLE_IMAGES_FORMATS') . '</div>
                        <div>&#8226; ' . Yii::t('app', 'IMAGE_SIZE_LIMIT_IS_5MB') . '</div>
                        <div>&#8226; ' . Yii::t('app', 'IMAGES_LIMIT_PER_UPLOAD_IS_20') . '</div>
                    </div>
                </div>
                <div class="fallback"></div>
            </div>
        </div>
    </div>
</div>
<div id="progress-bar" class="row">
	<div class="col-xs-12 col-sm-12 col-lg-12">
		<div class="progress">
			<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div>
		</div>
	</div>
</div>
<div id="send-button" class="row">
	<div class="col-xs-12 col-sm-12 col-lg-12">
		<div class="form-group text-center">
			<button class="btn btn-primary send" type="submit" disabled="disabled">
                <i class="glyphicon glyphicon-upload"></i>
                <span>' . Yii::t('app', 'SEND') . '</span>
			</button>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div id="previews" class="row">
	</div>
</div>
';

$previewTemplate =
    '
<div id="template" class="col-xs-4 col-sm-3 col-lg-2 dz-file-preview">
	<div class="thumbnail">
		<div class="error-message">
			<span data-dz-errormessage></span>
		</div>
		<img data-dz-thumbnail/>
		<span class="glyphicon glyphicon-picture picture"></span>
		<button data-dz-remove class="btn btn-danger delete">
			<i class="glyphicon glyphicon-trash"></i>
		</button>
		<div class="name" style="margin-top: 7px;" data-dz-name></div>
		<div class="size" data-dz-size></div>
	</div>
</div>
';
?>
<div class="user-photos-upload">

    <div class="text-center">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <?= Dropzone::widget([
        'dropzoneContainer' => '#dropzone',
        'options' => [
            'url' => 'upload',
            'parallelUploads' => 20,
            'uploadMultiple' => true,
            'maxFilesize' => 5,
            'thumbnailWidth' => 160,
            'thumbnailHeight' => 160,
            'filesizeBase' => 1024,
            'maxFiles' => 20,
            'acceptedFiles' => 'image/png,image/jpg,image/jpeg',
            'autoProcessQueue' => false,
            'previewsContainer' => '#previews',
            'previewTemplate' => $previewTemplate,
            'dictDefaultMessage' => '',
            'dictResponseError' => '',
            'dictFallbackMessage' => Yii::t('app', 'BROWSER_IS_NOT_SUPPORT_UPLOADING_IMAGES'),
            'dictFileTooBig' => Yii::t('app', 'IMAGE_SIZE_IS_TOO_BIG'),
            'dictInvalidFileType' => Yii::t('app', 'INVALID_FILE_FORMAT'),
            'dictMaxFilesExceeded' => Yii::t('app', 'IMAGES_LIMIT_PER_UPLOAD_IS_REACHED'),
        ],
        'events' => [
            'addedfile' =>
                "function(file) {
                    setTimeout(function() {
                        if (dropzone.getAcceptedFiles().length > 0) {
                            element = document.querySelector('#send-button .send');
                            element.removeAttribute('disabled');
                            element.onclick = function() {dropzone.processQueue()};
                        }
                    }, 100)
                }",
            'removedfile' =>
                "function(file) {
                    if (dropzone.getAcceptedFiles().length === 0) {
                        document.querySelector('#send-button .send').setAttribute('disabled', 'disabled');
                    }
                }",
            'processingmultiple' =>
                "function(files) {
                    document.querySelector('#dropzone').setAttribute('style', 'pointer-events: none;');
                    document.querySelector('#send-button .send').setAttribute('disabled', 'disabled');
                    
                    elements = document.querySelectorAll('#template.dz-processing .delete');
                    for (i = 0, length = elements.length; i < length; i++) {
                        element = elements[i];
                        element.parentNode.removeChild(element);
                    }
                }",
            'totaluploadprogress' =>
                "function(progress) {
                    if (dropzone.getAcceptedFiles().length > 0) {
                        progress = Math.round(progress);
                        element = document.querySelector('#progress-bar .progress-bar');
                        element.setAttribute('aria-valuenow', progress);
                        element.style.width = progress + '%';
                        
                        if (progress === 100) {
                            element.textContent = '" . Yii::t('app', 'PROCESSING_IMAGE_FILES_ON_THE_SERVER') . "';
                        } else {
                            element.textContent = progress + '%';
                        }
                    }
                }",
            'errormultiple' =>
                "function(files, message, xhr) {
                    var url = xhr && xhr.getResponseHeader('X-Redirect');
                    if (url) {
                        window.location = url;
                    }
                }",
        ],
        'template' => $template,
    ]) ?>

</div>
