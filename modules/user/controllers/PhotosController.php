<?php

namespace app\modules\user\controllers;

use app\modules\user\models\PhotosUploadForm;
use app\modules\user\models\User;
use Yii;
use app\modules\user\models\Photo;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PhotosController implements the CRUD actions for Photo model.
 */
class PhotosController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Photo models for user.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Photo::find()->byUserId(Yii::$app->user->identity->getId()),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a photos uploading form and handle it.
     * @return mixed
     */
    public function actionUpload()
    {
        if (Yii::$app->request->isAjax) {
            $userId = $this->findUserModel()->getId();
            $model = new PhotosUploadForm($userId);
            $result = $model->uploadPhotos();
            $errors = implode(PHP_EOL, $model->getErrors('files'));
            if ($result) {
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'IMAGE_FILES_UPLOADING_SUCCESS'));
                return $this->redirect('index');
            } else {
                if (is_array($result)) {
                    Yii::$app->getSession()->setFlash('danger', nl2br(Yii::t('app', 'SERVER_ERROR_PROCESSING_SOME_IMAGE_FILES') . PHP_EOL . $errors, false));
                    return $this->redirect('index');
                } else {
                    Yii::$app->getSession()->setFlash('error', nl2br(Yii::t('app', 'SERVER_ERROR_PROCESSING_IMAGE_FILES') . PHP_EOL . $errors, false));
                    return $this->refresh();
                }
            }
        }

        return $this->render('upload');
    }

    /**
     * Displays a single Photo model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findPhotoModel($id),
        ]);
    }

    /**
     * Creates a new Photo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Photo();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Photo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findPhotoModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Photo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findPhotoModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Photo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Photo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findPhotoModel($id)
    {
        if (($model = Photo::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the User model based on current login user.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    private function findUserModel()
    {
        if (($model = User::findOne(Yii::$app->user->identity->getId())) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
