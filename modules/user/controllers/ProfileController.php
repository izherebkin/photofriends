<?php

namespace app\modules\user\controllers;

use app\modules\user\models\ProfileUpdateForm;
use app\modules\user\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class ProfileController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index', [
            'model' => $this->findModel(),
        ]);
    }

    public function actionUpdate()
    {
        $user = $this->findModel();
        $model = new ProfileUpdateForm($user);
        if (Yii::$app->request->isPost) {
            if (Yii::$app->request->post(ProfileUpdateForm::SCENARIO_AVATAR) !== null) {
                $model->scenario = ProfileUpdateForm::SCENARIO_AVATAR;
                if ($model->load(Yii::$app->request->post()) && $model->uploadAvatar()) {
                    Yii::$app->getSession()->setFlash('success', Yii::t('app', 'NEW_AVATAR_SAVED'));
                    return $this->refresh();
                }

            }
            if (Yii::$app->request->post(ProfileUpdateForm::SCENARIO_EMAIL) !== null) {
                $model->scenario = ProfileUpdateForm::SCENARIO_EMAIL;
                if ($model->load(Yii::$app->request->post()) && $model->updateEmail()) {
                    Yii::$app->user->logout();
                    Yii::$app->getSession()->setFlash('success', Yii::t('app', 'SIGNUP_SUCCESS'));
                    return $this->goHome();
                }
            }
            if (Yii::$app->request->post(ProfileUpdateForm::SCENARIO_PASSWORD) !== null) {
                $model->scenario = ProfileUpdateForm::SCENARIO_PASSWORD;
                if ($model->load(Yii::$app->request->post()) && $model->updatePassword()) {
                    Yii::$app->user->logout();
                    Yii::$app->getSession()->setFlash('success', Yii::t('app', 'NEW_PASSWORD_SAVED'));
                    return $this->redirect(['default/login']);
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            'avatar' => $user->avatar,
            'username' => $user->username,
        ]);
    }

    /**
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    private function findModel()
    {
        if (($model = User::findOne(Yii::$app->user->identity->getId())) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}