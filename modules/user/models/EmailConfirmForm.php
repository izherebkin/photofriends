<?php

namespace app\modules\user\models;

use yii\base\InvalidParamException;
use yii\base\Model;

class EmailConfirmForm extends Model
{
    private $user;

    /**
     * Creates a form model given a token.
     *
     * @param  string $token
     * @param  array $config
     * @throws \yii\base\InvalidParamException if token is empty or not valid
     */
    public function __construct($token, $config = [])
    {
        if (empty($token) || !is_string($token)) {
            throw new InvalidParamException('Email confirm token cannot be blank.');
        }
        $this->user = User::findByEmailConfirmToken($token);
        if (empty($this->user)) {
            throw new InvalidParamException('Wrong email confirm token.');
        }
        parent::__construct($config);
    }

    /**
     * Confirm email.
     *
     * @return boolean if email was confirmed.
     */
    public function confirmEmail()
    {
        $this->user->status = User::STATUS_ACTIVE;
        $this->user->removeEmailConfirmToken();

        return $this->user->save();
    }
}