<?php

namespace app\modules\user\models;

use Yii;
use yii\base\InvalidParamException;
use yii\base\Model;


/**
 * Password reset form
 */
class PasswordResetForm extends Model
{
    public $password;

    private $user;


    /**
     * Creates a form model given a token.
     *
     * @param string $token
     * @param array $config name-value pairs that will be used to initialize the object properties
     * @throws \yii\base\InvalidParamException if token is empty or not valid
     */
    public function __construct($token, $config = [])
    {
        if (empty($token) || !is_string($token)) {
            throw new InvalidParamException('Password reset token cannot be blank.');
        }
        $this->user = User::findByPasswordResetToken($token);
        if (empty($this->user)) {
            throw new InvalidParamException('Password reset token invalid.');
        }
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['password', 'required'],
            ['password', 'string', 'min' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'password' => Yii::t('app', 'NEW_PASSWORD'),
        ];
    }

    /**
     * Resets password.
     *
     * @return bool if password was reset.
     */
    public function resetPassword()
    {
        $this->user->setPassword($this->password);
        $this->user->removePasswordResetToken();

        return $this->user->save(false);
    }
}