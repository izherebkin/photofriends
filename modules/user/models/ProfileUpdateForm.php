<?php

namespace app\modules\user\models;

use Exception;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\imagine\Image;
use yii\web\ServerErrorHttpException;
use yii\web\UploadedFile;

class ProfileUpdateForm extends Model
{
    const SCENARIO_AVATAR = 'avatar-change-button';
    const SCENARIO_EMAIL = 'email-change-button';
    const SCENARIO_PASSWORD = 'password-change-button';

    /**
     * @var UploadedFile
     */
    public $avatar;
    public $email;
    public $currentPassword;
    public $newPassword;
    public $newPasswordRepeat;

    /**
     * @var User
     */
    private $user;

    /**
     * @param User $user
     * @param array $config
     */
    public function __construct(User $user, $config = [])
    {
        $this->user = $user;
        $this->email = $user->email;
        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [
                'avatar',
                'file',
                'skipOnEmpty' => false,
                'extensions' => 'png, jpg, jpeg',
                'mimeTypes' => 'image/png, image/jpg, image/jpeg',
                'maxSize' => 1048576,
                'tooBig' => Yii::t('app', 'FILE_SIZE_LIMIT_IS_1MB'),
            ],
            ['avatar', 'image', 'minWidth' => 200, 'minHeight' => 200, 'maxWidth' => 1000, 'maxHeight' => 1000],
            ['avatar', 'validateSquareImage'],
            ['email', 'trim'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => User::className(), 'message' => Yii::t('app', 'IT_IS_YOUR_CURRENT_EMAIL')],
            ['email', 'unique', 'targetClass' => User::className(), 'message' => Yii::t('app', 'EMAIL_ALREADY_TAKEN'), 'filter' => ['!=', 'id', $this->user->id]],
            [['avatar', 'email', 'currentPassword', 'newPassword', 'newPasswordRepeat'], 'required'],
            ['currentPassword', 'validatePassword'],
            [['currentPassword', 'newPassword', 'newPasswordRepeat'], 'string', 'min' => 5],
            ['newPasswordRepeat', 'compare', 'compareAttribute' => 'newPassword'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'avatar' => Yii::t('app', 'AVATAR'),
            'email' => Yii::t('app', 'EMAIL'),
            'currentPassword' => Yii::t('app', 'CURRENT_PASSWORD'),
            'newPassword' => Yii::t('app', 'NEW_PASSWORD'),
            'newPasswordRepeat' => Yii::t('app', 'NEW_PASSWORD_REPEAT'),
        ];
    }

    /**
     * @param string $attribute
     * @param array $params
     */
    public function validateSquareImage($attribute, $params)
    {
        if (!$this->hasErrors()) {
            list($width, $height) = getimagesize($this->avatar->tempName);
            if ($width != $height) {
                $this->addError($attribute, Yii::t('app', 'IMAGE_MUST_BE_SQUARE'));
            }
        }
    }

    /**
     * @param string $attribute
     * @param array $params
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (!$this->user->validatePassword($this->$attribute)) {
                $this->addError($attribute, Yii::t('app', 'PASSWORD_INVALID'));
            }
        }
    }

    public function uploadAvatar()
    {
        $this->avatar = UploadedFile::getInstance($this, 'avatar');
        if ($this->validate()) {
            $basePath = Yii::getAlias('@webroot/upload/avatars/');
            $baseUrl = Yii::getAlias('@web/upload/avatars/');
            $fileName = Yii::$app->security->generateRandomString() . '.' . $this->avatar->extension;
            try {
                $image = Image::getImagine()->open($this->avatar->tempName);
                $image->thumbnail(new Box(200, 200))->save($basePath . $fileName, ['quality' => 90]);
            } catch (Exception $e) {
                throw new ServerErrorHttpException('An error occurred while processing the image on the server.', 500, $e);
            }
            $prevAvatar = Yii::$app->basePath . $this->user->avatar;
            $this->user->avatar = $baseUrl . $fileName;
            $save = $this->user->save();
            if ($save && file_exists($prevAvatar)) {
                unlink($prevAvatar);
            }
            return $save;
        }

        return false;
    }

    public function updateEmail()
    {
        if ($this->validate()) {
            $this->user->email = $this->email;
            $this->user->status = User::STATUS_WAIT;
            $this->user->generateAuthKey();
            $this->user->generateEmailConfirmToken();

            if ($this->user->save()) {
                Yii::$app->mailer->compose('@app/modules/user/mail/emailConfirm', ['user' => $this->user])
                    ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' Robot'])
                    ->setTo($this->user->email)
                    ->setSubject(Yii::t('app', 'CONFIRM_EMAIL_SUBJECT') . Yii::$app->name)
                    ->send();
                return true;
            }
        }

        return false;
    }

    public function updatePassword()
    {
        if ($this->validate()) {
            $this->user->setPassword($this->newPassword);
            return $this->user->save();
        }

        return false;
    }

    public function scenarios()
    {
        return ArrayHelper::merge(parent::scenarios(), [
            self::SCENARIO_AVATAR => ['avatar'],
            self::SCENARIO_EMAIL => ['email'],
            self::SCENARIO_PASSWORD => ['currentPassword', 'newPassword', 'newPasswordRepeat'],
        ]);
    }
}