<?php

namespace app\modules\user\models;

use Exception;
use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Yii;
use yii\base\Model;
use yii\imagine\Image;
use yii\web\UploadedFile;

class PhotosUploadForm extends Model
{
    /**
     * @var UploadedFile[]
     */
    public $files;

    /**
     * @var int an ID that uniquely identifies a user identity.
     */
    private $userId;

    /**
     * @param int $userId
     * @param array $config
     */
    public function __construct($userId, $config = [])
    {
        $this->userId = $userId;
        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [
                'files',
                'file',
                'skipOnEmpty' => false,
                'extensions' => 'png, jpg, jpeg',
                'mimeTypes' => 'image/png, image/jpg, image/jpeg',
                'maxSize' => 5242880,
                'maxFiles' => 20,
            ],
            ['files', 'image', 'maxFiles' => 20],
        ];
    }

    /**
     * @return array|bool true - if all image files, array - if at least one image file and false - if no one image file upload successful.
     */
    public function uploadPhotos()
    {
        $this->files = UploadedFile::getInstancesByName('file');
        if ($this->validate()) {
            $basePath = Yii::getAlias('@webroot/upload/photos/');
            $baseUrl = Yii::getAlias('@web/upload/photos/');
            $result = false;
            foreach ($this->files as $file) {
                $singleResult = false;
                $name = $file->name;
                $fileName = Yii::$app->security->generateRandomString();
                $originFileName = $fileName . '-original' . '.' . $file->extension;
                $thumbFileName = $fileName . '-thumbnail' . '.' . $file->extension;
                try {
                    $image = Image::getImagine()->open($file->tempName);

                    $originBox = new Box(1920, 1080);
                    $image->thumbnail($originBox, ImageInterface::THUMBNAIL_INSET)->save($basePath . $originFileName, ['quality' => 90]);

                    $thumbBox = new Box(160, 160);
                    if ($thumbBox->contains($image->getSize())) {
                        $min = min($image->getSize()->getWidth(), $image->getSize()->getHeight());
                        $thumbBox = new Box($min, $min);
                    }
                    $image->thumbnail($thumbBox, ImageInterface::THUMBNAIL_OUTBOUND)->save($basePath . $thumbFileName, ['quality' => 90]);
                    $singleResult = true;
                } catch (Exception $e) {
                    $this->addError('files', Yii::$app->getI18n()->format(Yii::t('app', 'SERVER_ERROR_PROSESSING_IMAGE_FILE'), ['file' => $name], Yii::$app->language));
                }

                if ($singleResult) {
                    $photo = new Photo();
                    $photo->user_id = $this->userId;
                    $photo->original = $baseUrl . $originFileName;
                    $photo->thumbnail = $baseUrl . $thumbFileName;
                    $photo->description = $name;
                    $save = $photo->save();
                    if ($save) {
                        if (!is_array($result)) {
                            $result = true;
                        }
                    } else {
                        $this->addError('files', Yii::$app->getI18n()->format(Yii::t('app', 'SERVER_ERROR_VALIDATION_FILE'), ['file' => $name], Yii::$app->language));
                        $singleResult = false;
                    }
                }

                if (!$singleResult) {
                    if ($result) {
                        $result = [];
                    }

                    $originPath = Yii::$app->basePath . $baseUrl . $originFileName;
                    $thumbPath = Yii::$app->basePath . $baseUrl . $thumbFileName;
                    if (file_exists($originPath)) {
                        unlink($originPath);
                    }
                    if (file_exists($thumbPath)) {
                        unlink($thumbPath);
                    }
                }
            }
            return $result;
        }

        return false;
    }

}