<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="main-default-error">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>

    <p>
        <?= Yii::t('app', 'WEBSERVER_REQUEST_ERROR') ?>
    </p>
    <p>
        <?= Yii::t('app', 'CONTACT_IF_REQUEST_ERROR_IS_NOT') ?>
    </p>

</div>
