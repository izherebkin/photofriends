<?php

namespace app\assets;

use yii\web\AssetBundle;

class DropzoneAsset extends AssetBundle
{
    public $sourcePath = '@bower/dropzone/dist';
    public $css = [
        'min/dropzone.min.css',
    ];
    public $js = [
        'min/dropzone.min.js',
    ];
}